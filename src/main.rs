// main.rs
//
// Copyright 2019 Alberto Ruiz <aruiz@redhat.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later

extern crate json;

use std::collections::HashMap;
use std::process::Command;
use std::io;

struct Disk {
  pub dev:  String
}

struct Partition {
  pub dev: String,
  parent: String,
  label: String,
  json_repr: json::JsonValue
}

impl Partition {
  fn is_dos_active (&self) -> bool {
    if self.label == "gpt" {
      let dos_parsed = get_json_from_device(&self.parent, true);
      if dos_parsed.is_err() {
        return false;
      }
      let dos_parsed = dos_parsed.unwrap();

      if self.json_repr["start"] != dos_parsed["start"] {
        return false;
      }

      if self.json_repr["size"] != dos_parsed["size"] {
        return false;
      }
    }
    self.dev.starts_with("/dev/") && { self.json_repr["bootable"] == true }
  }

  fn is_mounted (&self) -> Option<String> {
    let output = match Command::new("/usr/bin/findmnt")
      .arg("-Uno")
      .arg("target")
      .arg(&self.dev)
      .output() {
        Ok(output) => output,
        Err(_) => return None
    };
    if output.status.success() {
      Some(unsafe { String::from_utf8_unchecked(output.stdout).trim().to_string() })
    } else {
      None
    }
  }

  fn get_part_info (&self) -> Option<HashMap<String,String>> {
    let mut result = HashMap::new();
    let output = match Command::new("/usr/bin/udevadm")
      .arg("info").arg("-q").arg("property").arg("-n").arg(&self.dev).output() {
        Ok(output) => output,
        Err(_) => return None
    };
    if ! output.status.success() {
      return None;
    }

    let output = unsafe {String::from_utf8_unchecked(output.stdout)};
    for line in output.trim().lines().filter(|x| {x.contains("=")}) {
      let mut tuple: Vec<_> = line.splitn(2,'=').map(|x| {String::from(x)}).collect();
      let value = tuple.pop().unwrap();
      let key = tuple.pop().unwrap();
      result.insert(key, value);
    }
    Some(result)
  }

}

impl Disk {
  fn list_all () -> io::Result<Vec<Self>> {
    let mut result = Vec::new();
    let output = Command::new("/usr/bin/lsblk")
      .args(&["-npo","kname,type"])
      .output()?;
    if ! output.status.success() {
        return Ok(result);
    }
    let stdout = unsafe { String::from_utf8_unchecked(output.stdout) };
    let lines = stdout.trim()
          .split("\n")
          .map(|x| x.split_whitespace())
          .map(|x| -> Vec<_> { x.collect() } );
    for line in lines {
      if line.len() < 2 {
        continue;
      }
      if line[1] != "disk" {
        continue;
      }
      if ! line[0].starts_with("/dev/") {
        continue;
      }

      result.push(Disk{dev: String::from(line[0])});
    }
    Ok(result)
  }

  fn partitions (&self) -> io::Result<Vec<Partition>> {
    let mut disk = get_json_from_device(&self.dev, false)?;
    let label = get_label_from_parttable (&mut disk)?;

    Ok(disk["partitiontable"]["partitions"].members_mut().map(|part| {
      Partition {dev: part["node"].to_string(), parent: self.dev.clone(), label: label.clone(), json_repr: part.clone()}
    }).collect())
  }
}

fn get_json_from_device(device: &String, from_mbr: bool) -> io::Result<json::JsonValue> {
  let mut label_data_comm = Command::new("/usr/sbin/sfdisk");
  label_data_comm.arg("--json");
  if from_mbr {
    label_data_comm.arg("--label-nested").arg("mbr");
  }

  label_data_comm.arg(device);

  let label_data = label_data_comm.output()?;
  if ! label_data.status.success() {
    return Ok({ eprintln!("Could not get partition data from {}: {}", device, String::from_utf8_lossy(&label_data.stderr).trim()); json::JsonValue::Null});
  }

  match json::parse(&String::from_utf8_lossy(&label_data.stdout)) {
    Ok(result) => { Ok(result) },
    Err(_) => {
      eprintln!("Could not parse sfdisk json partition data from {}", device);
      Ok(json::JsonValue::Null)
    }
  }
}

fn get_label_from_parttable (value: &mut json::JsonValue) -> io::Result<String> {
  if value["partitiontable"]["label"].is_null() || ! value["partitiontable"]["label"].is_string() {
    return Err(io::Error::new(io::ErrorKind::InvalidData, format!("Invalid json output from sfdisk: {}", value)));
  }
  Ok(value["partitiontable"]["label"].take_string().unwrap())
}

fn mounted_list_dir(path: &[&str], prefix: &str, dev: &str) -> bool {
  if let Ok(entries) = std::fs::read_dir(prefix) {
    for entry in entries {
      if let Ok(entry) = entry {
        if let Some(fname) = entry.file_name().to_str() {
          if fname.to_lowercase() == path[0] {
            return match path.len() {
              1 => true,
              _ => mounted_list_dir(&path[1..], format!("{}/{}", prefix, fname).as_str(), dev)
            };
          }
        }
      }
    }
  };
  false
}

fn probe_mounted (dev: &str, prefix: &str, files: &[&str]) -> bool {
  files.iter().map(|file| {
    let lower = file.to_lowercase();
    let path: Vec<&str> = lower.split("/").collect();
    mounted_list_dir(&path, prefix, dev)
  }).all(|found| found)
}

fn ntfs_list_dir(path: &[&str], prefix: &str, dev: &str) -> bool {
  if path.len() < 1 { return true; }
  let output = match Command::new("/usr/bin/ntfsls")
    .arg("-f")
    .arg("-a")
    .arg("-p")
    .arg(prefix)
    .arg(dev).output() {
    Ok(output) => { Some(output) },
    _ => { None }
  };

  match output {
    Some(output) => {
      let stdout = unsafe { String::from_utf8_unchecked(output.stdout) };
      if path.len() == 1 && &stdout[1..1] == "." {
        return false; //We only look for files, not dirs
      }
      { if &stdout[1..1] == "." { &stdout[2..] } else { &stdout } }
        .trim()
        .split("\n")
        .map(|entry| {
            let low = entry.to_lowercase(); // normalize to lowercase
            if low == path[0] {
              let next_prefix = format!("{}/{}", prefix, entry);
              ntfs_list_dir(&path[1..], &next_prefix, dev)
            } else {
              false
            }
          }).filter(|result| *result == true).count() > 0
        }
    _ => false
  }
}

fn ntfs_probe (dev: &str, files: &[&str]) -> bool {
  files.iter()
    .map(|file| {
    let lower = file.to_lowercase(); // normalize to lowercase
    let path: Vec<&str> = lower.split("/").collect();
    ntfs_list_dir(&path, "/", &dev)
  }).all(|found| found)
}

/*
fn mtools_probe (dev: &str, files: &[&str]) -> bool {
  files.iter().map(|file| {
      Command::new("/usr/bin/mdir")
        .arg("-i")
        .arg(dev)
        .arg("-f")
        .arg("-b")
        .stdout(std::process::Stdio::null())
        .stderr(std::process::Stdio::null())
        .arg(format!("::/{}", file))
        .status()})
    .filter(|result| { result.is_ok() })
    .map(|result| { result.unwrap() })
    .all(|result| { result.success() })
}

fn ext2_probe (dev: &str, basedirs: &[&str], file: &str) -> Option<String> {
  for base in basedirs {
    let candidate = format!("/{}/{}", base, file);
    match Command::new("/usr/bin/e2ls")
      .arg("-d")
      .arg(dev)
      .arg(&candidate)
      .stdout(std::process::Stdio::null())
      .stderr(std::process::Stdio::null())
      .status() {
      Ok(result) => { if result.success() { return Some(candidate); } }
      Err(_) => { eprintln!("Could not execute e2ls in {}", dev); }
    }
  };
  None
}
*/

fn probe_windows (part: &Partition) {
  const CONFIGSYS: [&str; 2] = ["CONFIG.SYS", "AUTOEXEC.BAT"];
  const NTLDR: [&str; 2] = ["ntldr", "boot.ini"];
  const BOOTMGR: [&str; 2] = ["Boot/BCD", "bootmgr"];

  let dev = &part.dev;
  let mnt = part.is_mounted();

  if ! part.is_dos_active() {
    return;
  }

  let info = match part.get_part_info() {
      Some(info) => info,
      _ => { return; }
  };

  if let (Some(fs), Some(ptype), Some(pnum)) = (info.get("ID_FS_TYPE"), info.get("UDISKS_PARTITION_TYPE"), info.get("UDISKS_PARTITION_NUMBER")) {
    if ptype.to_uppercase() == "0xEF" || ptype.to_uppercase() == "C12A7328-F81F-11D2-BA4B-00A0C93EC93B" {
      return;
    }

    if ! "1234".contains(pnum) {
      return;
    }

    match fs.as_str() {
      "vfat" | "ntfs" => {},
      _ => return
    };

    fn probe_mounted_all_versions(dev: &str, fs: &str, mountpoint: &str) -> &'static str {
      if probe_mounted(dev, &mountpoint, &BOOTMGR) { "Microsoft Windows Vista/7/8/10" }
      else if probe_mounted(dev, &mountpoint, &NTLDR) { "Microsoft Windows NT/2000/XP" }
      else if fs == "vfat" && probe_mounted(dev, &mountpoint, &CONFIGSYS) { "Microsoft 95/98/ME" }
      else { "" }
    };

    fn probe_by_mount (part: &Partition, dev: &str, fs: &str) -> &'static str {
      let mountpoint = format!("/tmp/__{}", part.dev.as_str().replace("/", "_"));
      let is_mounted = match std::fs::create_dir_all(&mountpoint) {
        Ok(()) => {
          mount(&part.dev, &mountpoint)
        }
        Err(ref e) if e.kind() == io::ErrorKind::AlreadyExists => {
          mount(&part.dev, &mountpoint)
        }
        _ => { false }
      };

      if is_mounted {
        let result = probe_mounted_all_versions(dev, fs, &mountpoint);
        let _ = Command::new("/usr/bin/sync").status();
        umount(&mountpoint);
        result
      } else {
        ""
      }
    }

    let outstr = match mnt {
      Some(mountpoint) => {
        probe_mounted_all_versions(dev, fs, &mountpoint)
      },
      _ => {
        match fs.as_str() {
          "ntfs" => {
            if std::path::Path::new("/usr/bin/ntfsls").is_file() {
              if ntfs_probe(dev, &BOOTMGR) { "Microsoft Windows Vista/7/8/10" }
              else if ntfs_probe(dev, &NTLDR) { "Microsoft Windows NT/2000/XP" }
              else { probe_by_mount(part, dev, fs) }
            } else {
              probe_by_mount(part, dev, fs)
            }
          },
          "vfat" => {
            probe_by_mount(part, dev, fs)
          },
          _ => { "" }
        }
      }
    };
    if outstr.len() == 0 {
      return;
    }
    println!("{}:{}:chain", dev, outstr);
  }
}

fn mount (dev: &str, mnt: &str) -> bool {
  match Command::new("/usr/bin/mount")
    .arg("-r")
    .arg(dev)
    .arg(mnt)
    .stdin(std::process::Stdio::null())
    .status() {
    Ok(status) => {
      status.success()
    }
    Err(_) => false
  }
}

fn umount (path: &str) -> bool {
  match Command::new("/usr/bin/umount")
    .arg("-R")
    .arg("-f")
    .arg(path)
    .stdin(std::process::Stdio::null())
    .status() {
    Ok(status) => {
      status.success()
    }
    Err(_) => false
  }
}


#[cfg(target_arch = "x86_64")]
const BOOT_SECOND_STAGE: &str = "i386-pc/core.img";
#[cfg(target_arch = "x86")]
const BOOT_SECOND_STAGE: &str = "i386-pc/core.img";
#[cfg(target_arch = "powerpc64")]
const BOOT_SECOND_STAGE: &str = "powerpc-ieee1275/core.img";
const BOOT_PREFIX: &[&str] = &["grub2", "boot/grub2", "grub", "boot/grub"];

fn probe_grub2_mounted (mountpoint: &String) -> Option<String> {
  if mountpoint == "/" || mountpoint == "/boot" {
    return None;
  }

  let candidates = BOOT_PREFIX.iter().map(|prefix| {
    let path = std::path::Path::new(&mountpoint).join(prefix);
    if path.is_dir() { Some(path) }
    else { None }
  })
    .filter(|path| path.is_some())
    .map(|path| path.unwrap());

  let mut found = None;
  for basepath in candidates {
    let current = basepath.join(BOOT_SECOND_STAGE);
    if current.is_file() {
      let fullpath = current.to_string_lossy();
      found = if mountpoint == "/" {
        Some(String::from(fullpath))
      } else {
        Some(String::from(&fullpath[mountpoint.len()..]))
      };
      break
    }
  }
  found
}

fn probe_linux_grub2 (part: &Partition) {
  let info = match part.get_part_info() {
      Some(info) => info,
      _ => { return; }
  };

  let fs = match info.get("ID_FS_TYPE") {
    Some(fs) => fs,
    None => { return; }
  };

  match part.is_mounted() {
    Some(mountpoint) => {
      if let Some(core_img) = probe_grub2_mounted(&mountpoint) {
        println!("{}:Linux:grubcore {}", part.dev, core_img);
      };
    }
    None => {
      match fs.as_str() {
        // Overlap between available kernel modules and GRUB2 fs modules
        "ext2" | "ext3" | "ext4" | "xfs" | "btrfs" | "zfs" | "jfs" | "reiserfs" | "ufs" | "minix" | "nilfs2" => {
          let mountpoint = format!("/tmp/__{}", part.dev.as_str().replace("/", "_"));
          let is_mounted = match std::fs::create_dir_all(&mountpoint) {
            Ok(()) => {
              mount(&part.dev, &mountpoint)
            }
            Err(ref e) if e.kind() == io::ErrorKind::AlreadyExists => {
              mount(&part.dev, &mountpoint)
            }
            _ => { false }
          };

          if is_mounted {
            if let Some(core_img) = probe_grub2_mounted(&mountpoint) {
              println!("{}:Linux:grubcore {}", part.dev, core_img);
            };
            let _ = Command::new("/usr/bin/sync").status();
            umount(&mountpoint);
          }
        },
        _ => {}
      }
    }
  }
}

fn is_efi_system() -> bool {
  std::path::Path::new("/sys/firmware/efi").is_dir()
}

fn main() {
  if is_efi_system() && std::env::var("OSPROBE_IGNORE_EFI").is_err() {
    return;
  }

  let disks = Disk::list_all().unwrap_or_else(|error| {
    eprintln!("Could not list disks: {}", error);
    std::process::exit(1);
  });

  let partitions: Vec<Partition> = disks.iter().map(  |disk| {
    match disk.partitions() {
      Ok(partitions) => partitions,
      Err(_) => {
        std::process::exit(1);
      }
    }
  })
  .flat_map(|disk_partitions| disk_partitions)
  .collect();

  for part in partitions {
    probe_windows(&part);
    probe_linux_grub2(&part)
  }
}
