os-prober-lite
--------------

This program is an attempt at creating a less intrusive and more limited version
of [os-prober](https://joeyh.name/code/os-prober/). os-prober is very aggressive
in that it tries to mount even lvm partitions or using FUSE based mounters like
ntfs-3g.

os-prober-lite tries to be a lot more conservative and limited in scope, for
example, it only looks for /boot content rather than trying to find the root
partition for every Linux distro (which is a problem with LUKS and LVM based
partitions). For NTFS partitions it tries to use userland tools like ntfsls
instead of mounting as it is known to cause problems of the system being blocked.

os-prober-lite also ignores EFI systems, since efi chainloading does not work
on GRUB anyway and it is not even possible on SecureBoot setups.